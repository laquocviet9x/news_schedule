package vnu.edu.news_schedule.repository.specification;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import vnu.edu.news_schedule.entity.ScheduleEntity;
import vnu.edu.news_schedule.entity.dto.ScheduleSearchRequestDTO;

public class ScheduleSpecification implements Specification<ScheduleEntity> {

  private ScheduleSearchRequestDTO requestDTO;

  public ScheduleSpecification(ScheduleSearchRequestDTO requestDTO) {
    this.requestDTO = requestDTO;
  }

  @Override
  public Predicate toPredicate(Root<ScheduleEntity> root, CriteriaQuery<?> query,
      CriteriaBuilder criteriaBuilder) {
    List<Predicate> predicates = new LinkedList<>();
    if (requestDTO.getDate() != null) {
      predicates.add(criteriaBuilder.equal(root.get("date"), this.requestDTO.getDate()));
    }
    if (requestDTO.getFromDate() != null) {
      predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("date"), this.requestDTO.getFromDate()));
    }
    if (requestDTO.getToDate() != null) {
      predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("date"), this.requestDTO.getToDate()));
    }
    return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
  }
}
