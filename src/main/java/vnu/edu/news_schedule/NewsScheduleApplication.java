package vnu.edu.news_schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;

@SpringBootApplication
public class NewsScheduleApplication {

  public static void main(String[] args) {
    SpringApplication.run(NewsScheduleApplication.class, args);
  }

}
