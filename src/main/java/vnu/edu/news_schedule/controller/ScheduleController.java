package vnu.edu.news_schedule.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vnu.edu.news_schedule.entity.dto.ScheduleSearchRequestDTO;
import vnu.edu.news_schedule.service.ScheduleService;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class ScheduleController {

  private final ScheduleService scheduleService;

  @GetMapping(value = "/v1.0/schedule")
  public ResponseEntity<?> findAll(ScheduleSearchRequestDTO requestDTO) {
    return ResponseEntity.ok(this.scheduleService.findAll(requestDTO));
  }

  @PostMapping(value = "/v1.0/schedule/import-excel")
  public ResponseEntity<?> importExcel(@RequestPart(value = "file") MultipartFile file) throws IOException {
    return ResponseEntity.ok(this.scheduleService.importExcel(file));
  }
}
