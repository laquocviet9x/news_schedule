package vnu.edu.news_schedule.service;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
import vnu.edu.news_schedule.entity.dto.ScheduleDTO;
import vnu.edu.news_schedule.entity.dto.ScheduleSearchRequestDTO;

import java.io.IOException;

public interface ScheduleService {

  Page<ScheduleDTO> findAll(ScheduleSearchRequestDTO requestDTO);

  String importExcel(MultipartFile file) throws IOException;

}
