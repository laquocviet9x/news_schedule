package vnu.edu.news_schedule.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vnu.edu.news_schedule.entity.ScheduleEntity;
import vnu.edu.news_schedule.entity.dto.ScheduleDTO;
import vnu.edu.news_schedule.entity.dto.ScheduleSearchRequestDTO;
import vnu.edu.news_schedule.repository.ScheduleRepository;
import vnu.edu.news_schedule.repository.specification.ScheduleSpecification;
import vnu.edu.news_schedule.service.ScheduleService;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ScheduleServiceImpl implements ScheduleService {

    private final ScheduleRepository scheduleRepository;

    private final ModelMapper modelMapper;

    @Override
    public Page<ScheduleDTO> findAll(ScheduleSearchRequestDTO requestDTO) {
        Pageable pageable = PageRequest.of(requestDTO.getPage(), requestDTO.getSize());
        ScheduleSpecification specification = new ScheduleSpecification(requestDTO);
        Page<ScheduleEntity> page = this.scheduleRepository.findAll(specification, pageable);
        return new PageImpl<>(
                page.getContent().stream().map(e -> this.modelMapper.map(e, ScheduleDTO.class))
                        .collect(Collectors.toList()),
                pageable, page.getTotalElements());
    }

    @Override
    public String importExcel(MultipartFile file) throws IOException {
        List<ScheduleEntity> list = this.excelToSchedules(file.getInputStream());
        this.scheduleRepository.saveAll(list);
        return "Upload file thành công!";
    }

    private List<ScheduleEntity> excelToSchedules(InputStream inputStream) {
        List<ScheduleEntity> list = new ArrayList<>();
        try {
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();
                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellsInRow = currentRow.iterator();
                ScheduleEntity entity = new ScheduleEntity();
                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();
                    switch (cellIdx) {
                        case 0:
                            entity.setScheduleId((int) currentCell.getNumericCellValue());
                            break;
                        case 1:
                            entity.setDate(LocalDate.parse(currentCell.getStringCellValue()));
                            break;
                        case 2:
                            entity.setTimeStart(LocalTime.parse(currentCell.getStringCellValue()));
                            break;
                        case 3:
                            entity.setContent(currentCell.getStringCellValue());
                            break;
                        case 4:
                            entity.setDescription(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                list.add(entity);
            }
            workbook.close();
            return list;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        } catch (Exception e) {
            return list;
        }
    }
}
