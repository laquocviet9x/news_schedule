package vnu.edu.news_schedule.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "news_schedule")
@Entity
public class ScheduleEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "schedule_id")
  private Integer scheduleId;

  @Column(name = "date")
  private LocalDate date;

  @Column(name = "time_start")
  private LocalTime timeStart;

  @Column(name = "content")
  private String content;

  @Column(name = "description")
  private String description;
}
