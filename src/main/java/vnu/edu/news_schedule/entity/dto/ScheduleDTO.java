package vnu.edu.news_schedule.entity.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDTO {

  private Integer scheduleId;

  private LocalDate date;

  private LocalTime timeStart;

  private String content;

  private String description;
}
