package vnu.edu.news_schedule.entity.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleSearchRequestDTO {

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate date;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate fromDate;

  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private LocalDate toDate;

  private int page = 0;

  private int size = 15;

}
